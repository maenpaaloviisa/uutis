import { get, setupTest } from '@nuxt/test-utils'

describe('ssr', () => {

  setupTest({ server: true })

  it('Otsikko näkyy oikein etusivulla.', async () => {
    const { body } = await get('/')
    expect(body).toContain('<h1 class="text-5xl font-bold"> Uutissovellus  </h1>')
  })

})
