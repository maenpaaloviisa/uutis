## Hello

Uutissovellus (v.0) on alusta, jossa voi katsoa uutisia aiheittain eri medioista. Mediat ovat etukäteen valittuja (New York Times, Hs.fi, Le Monde, Il Messaggero, Il Reppublica)
Koska mediat ovat nyt fiksattuja -lopputulos ei ole kovin objektiivinen- eli voisi olla hyvä, jos ne arvotaan satunnaisesti esimerkiksi jokaisella sivun uudelleenlataamisella.
Kielen valinta, mahdollisuus tallentaa käyttäjän asetukset ilman kirjautumista, parempi valikoima aiheita ja New York Times:in otsikon symbolin korjaus ovat kehitysehdotuksiamme.
